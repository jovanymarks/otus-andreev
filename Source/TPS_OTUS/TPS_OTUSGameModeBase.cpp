// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_OTUSGameModeBase.h"

ATPS_OTUSGameModeBase::ATPS_OTUSGameModeBase()
{
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Character/BP_TPSO_BaseCharacter"));
    if (PlayerPawnBPClass.Class != NULL)
    {
        DefaultPawnClass = PlayerPawnBPClass.Class;
    }
}