// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_OTUSGameModeBase.generated.h"

UCLASS()
class TPS_OTUS_API ATPS_OTUSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_OTUSGameModeBase();
};
