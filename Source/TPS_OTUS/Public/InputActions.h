#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputAction.h"
#include "InputActions.generated.h"

UCLASS()
class TPS_OTUS_API UInputActions : public UDataAsset
{
    GENERATED_BODY()

public:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UInputAction *InputMove;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UInputAction *InputLook;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UInputAction *InputJump;
};