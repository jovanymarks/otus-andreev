// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_OTUS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS_OTUS, "TPS_OTUS" );
